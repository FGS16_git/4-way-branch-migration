# Overcoming the speed limit of four-way DNA branch migration with bulges in the toeholds - Francesca Smith, Aditya Sengar, John Goertz, Molly M. Stevens, Guy-Bart Stan, Thomas Ouldridge and Wooli Bae

## Project Description
This project includes an example code for fitting an ODE model to kinetic data from experiments of 4-way branch migration. This code was used for fitting the data derived for the following paper: Overcoming the speed limit of four-way DNA branch migration with  bulges in toeholds. 

## Instructions for running the data analysis code 

There are 4 files distinct code files which are required to recreate the data analysis within the paper. We provide DNA-DNA_4-way_branch_migration_with_blockers_no_bulge_(22nt).ipynb as an example, as the code for data analysis is the same across all experimental input data within this work. Input data is in the format of normalised fluorescence data within a csv file. 

The code file can be opened and run in jupyter notebook. All packages required to run the codes come bundled with Anaconda Python 
(download link: https://www.anaconda.com/products/individual#windows). The Anaconda version 4.9.2 has been used for this work. The specific package versions used are indicated at the end of the notebook file, 
but in general they are:

Python version : 3.8.8
IPython version : 7.22.0

matplotlib : 3.3.4 
scipy : 1.6.2
numpy : 1.20.1
pandas : 1.2.4
re : 2.2.1
jax : 0.2.12
seaborn : 0.11.1
logging : 0.5.1.2
sys : 3.8.8
watermark : 2.2.0

## Support
For support or advice related to this work please contact either Francesca Smith (fgs16@imperial.ac.uk) or Dr Wooli Bae (w.bae@surrey.ac.uk).
